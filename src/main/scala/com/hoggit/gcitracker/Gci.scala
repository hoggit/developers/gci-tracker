package com.hoggit.gcitracker

import org.joda.time.DateTime
import spray.json._

trait GCIStatus {
  val status: String
}
case class Sunrise() extends GCIStatus {
  val status = "Sunrise"
}
case class Midnight() extends GCIStatus {
  val status = "Midnight"
}

object GCIStatus {
  implicit object GCIStatusJsonFormat extends RootJsonFormat[GCIStatus] {
    def write(g: GCIStatus) = g match {
      case Sunrise() => JsString("Sunrise")
      case Midnight() => JsString("Midnight")
    }

    def read(js: JsValue) = js match {
      case JsString("Midnight") => Midnight()
      case JsString("Sunrise") => Sunrise()
      case _ => throw new RuntimeException("Expected 'Midnight' or 'Sunrise' as the status but got neither. Cannot continue")
    }
  }
}

object GCIEvent {
  import DefaultJsonProtocol._
  implicit object dateTimeJsonFormat extends RootJsonFormat[DateTime] {
    def write(dt: DateTime): JsValue = JsString(dt.toString())
    def read(value: JsValue): DateTime = value match {
      case JsString(s) =>
        try {
          DateTime.parse(s)
        } catch {
          case _: Throwable =>
            deserializationError("DateTime expected")
        }
      case _ =>
        deserializationError("DateTime expected")
    }
  }
  implicit val GCIEventJsonFormat: RootJsonFormat[GCIEvent] = jsonFormat4(GCIEvent.apply)

}
case class GCIEvent(name: String, discordId: String, timestamp: DateTime, status: GCIStatus)
