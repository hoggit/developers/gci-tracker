package com.hoggit.gcitracker

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

object QuickstartServer extends App with StrictLogging {
  implicit val config = ConfigFactory.load()
  implicit val system = ActorSystem("gcitracker")
  logger.info("Server ready")
}
