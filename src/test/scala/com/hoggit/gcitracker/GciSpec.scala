package com.hoggit.gcitracker

import org.joda.time.DateTime
import org.scalatest.{ FlatSpec, Matchers }
import spray.json._

class GciSpec extends FlatSpec with Matchers {

  "GciStatus" should "convert to and from json" in {
    val timestamp = DateTime.now()
    val status = GCIEvent("Acidic", "Acidic#0001", timestamp, Midnight())
    val jsonStatus = status.toJson.compactPrint
    status.toJson shouldBe jsonStatus.parseJson
  }

}
